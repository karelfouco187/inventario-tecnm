import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarEscritoriosComponent } from './buscar-escritorios.component';

describe('BuscarEscritoriosComponent', () => {
  let component: BuscarEscritoriosComponent;
  let fixture: ComponentFixture<BuscarEscritoriosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarEscritoriosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuscarEscritoriosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
