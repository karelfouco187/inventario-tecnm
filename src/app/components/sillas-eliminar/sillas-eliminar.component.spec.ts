import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SillasEliminarComponent } from './sillas-eliminar.component';

describe('SillasEliminarComponent', () => {
  let component: SillasEliminarComponent;
  let fixture: ComponentFixture<SillasEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SillasEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SillasEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
