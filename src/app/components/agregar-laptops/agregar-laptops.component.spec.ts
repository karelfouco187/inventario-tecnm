import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarLaptopsComponent } from './agregar-laptops.component';

describe('AgregarLaptopsComponent', () => {
  let component: AgregarLaptopsComponent;
  let fixture: ComponentFixture<AgregarLaptopsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarLaptopsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgregarLaptopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
