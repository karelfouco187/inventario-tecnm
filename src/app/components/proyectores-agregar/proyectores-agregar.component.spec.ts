import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoresAgregarComponent } from './proyectores-agregar.component';

describe('ProyectoresAgregarComponent', () => {
  let component: ProyectoresAgregarComponent;
  let fixture: ComponentFixture<ProyectoresAgregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoresAgregarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProyectoresAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
