import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletsEliminarComponent } from './tablets-eliminar.component';

describe('TabletsEliminarComponent', () => {
  let component: TabletsEliminarComponent;
  let fixture: ComponentFixture<TabletsEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabletsEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabletsEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
