import { Component, OnInit } from '@angular/core';
import { tablets } from 'src/app/models/tablets';
import { TabletsService } from 'src/app/services/tablets.service';
@Component({
  selector: 'app-tablets-eliminar',
  templateUrl: './tablets-eliminar.component.html',
  styleUrls: ['./tablets-eliminar.component.css']
})
export class TabletsEliminarComponent implements OnInit {
  listTablets: tablets[]=[];
  constructor(private _tabletsService: TabletsService){}
  ngOnInit(): void {
    this.obtenerTablets();
  }
  obtenerTablets(){
    this._tabletsService.getTablets().subscribe(data =>{
      console.log(data);
      this.listTablets=data;
    },error =>{
      console.log(error);
    })
  }
  eliminarEquipos(id:any){
    this._tabletsService.eliminarTablets(id).subscribe(data =>{
      console.log("eliminado");
      this.obtenerTablets();
    })
  }
}
