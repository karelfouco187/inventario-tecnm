import { Component, OnInit } from '@angular/core';
import { tablets } from 'src/app/models/tablets';
import { TabletsService } from 'src/app/services/tablets.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-tablets-agregar',
  templateUrl: './tablets-agregar.component.html',
  styleUrls: ['./tablets-agregar.component.css']
})
export class TabletsAgregarComponent implements OnInit {
  tabletForm: FormGroup;
  id: string | null;
  titulo = 'Insertar Tablet';

  constructor(private fb: FormBuilder,
    private _tabletsService: TabletsService,
    private aRouter: ActivatedRoute,) {

    this.tabletForm = this.fb.group({
      serie: ['', Validators.required],
      imei: ['', Validators.required],
      marca: ['', Validators.required],
      ram: ['', Validators.required],
      almacenamiento: ['', Validators.required],
    })
    this.id = this.aRouter.snapshot.paramMap.get('id');
    }

  ngOnInit(): void {
    this.esEditar();
  }
  agregarTablet() {
    console.log(this.tabletForm);
    const TABLET: tablets={
      serie: this.tabletForm.get('serie')?.value,
      imei: this.tabletForm.get('imei')?.value,
      marca: this.tabletForm.get('marca')?.value,
      ram: this.tabletForm.get('ram')?.value,
      almacenamiento: this.tabletForm.get('almacenamiento')?.value,
    }

    if(this.id !==null){
      //editamos producto
      this._tabletsService.editarTablets(this.id,TABLET).subscribe(data =>{
        console.log("actualizado");
      }, error =>{
        console.log(error);
        this.tabletForm.reset();
      })
    }
    else{
      //agregamos producto
      console.log(TABLET);
      this._tabletsService.guardarTablets(TABLET).subscribe(data =>{
        console.log("Equipo Guardado");
      }, error =>{
        console.log(error);
        this.tabletForm.reset();
      })
    }
  }
  esEditar(){
    if(this.id !== null){
      this.titulo= 'Editar Tablet';
      this._tabletsService.obtenerTablets(this.id).subscribe(data =>{
        this.tabletForm.setValue({
          serie: data.serie,
          imei:data.imei,
          marca:data.marca,
          ram:data.ram,
          almacenamiento:data.almacenamiento,
        })
      })
    }
  }
}
