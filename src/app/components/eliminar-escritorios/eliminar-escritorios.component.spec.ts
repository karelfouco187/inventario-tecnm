import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarEscritoriosComponent } from './eliminar-escritorios.component';

describe('EliminarEscritoriosComponent', () => {
  let component: EliminarEscritoriosComponent;
  let fixture: ComponentFixture<EliminarEscritoriosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminarEscritoriosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EliminarEscritoriosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
