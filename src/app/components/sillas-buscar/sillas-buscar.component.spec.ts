import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SillasBuscarComponent } from './sillas-buscar.component';

describe('SillasBuscarComponent', () => {
  let component: SillasBuscarComponent;
  let fixture: ComponentFixture<SillasBuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SillasBuscarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SillasBuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
