import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SillasAgregarComponent } from './sillas-agregar.component';

describe('SillasAgregarComponent', () => {
  let component: SillasAgregarComponent;
  let fixture: ComponentFixture<SillasAgregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SillasAgregarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SillasAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
