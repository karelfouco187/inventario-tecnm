import { Component, OnInit } from '@angular/core';
import { tablets } from 'src/app/models/tablets';
import { TabletsService } from 'src/app/services/tablets.service';
@Component({
  selector: 'app-tablets-actualizar',
  templateUrl: './tablets-actualizar.component.html',
  styleUrls: ['./tablets-actualizar.component.css']
})
export class TabletsActualizarComponent implements OnInit  {
  listTablets: tablets[]=[];
  constructor(private _tabletsService: TabletsService){}

  ngOnInit(): void {
    this.obtenerTablets();
  }
  obtenerTablets(){
    this._tabletsService.getTablets().subscribe(data =>{
      console.log(data);
      this.listTablets=data;
    },error =>{
      console.log(error);
    })
  }
}
