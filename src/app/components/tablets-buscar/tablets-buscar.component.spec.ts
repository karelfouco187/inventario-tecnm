import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletsBuscarComponent } from './tablets-buscar.component';

describe('TabletsBuscarComponent', () => {
  let component: TabletsBuscarComponent;
  let fixture: ComponentFixture<TabletsBuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabletsBuscarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabletsBuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
