import { Component, OnInit } from '@angular/core';
import { tablets } from 'src/app/models/tablets';
import { TabletsService } from 'src/app/services/tablets.service';
@Component({
  selector: 'app-tablets-buscar',
  templateUrl: './tablets-buscar.component.html',
  styleUrls: ['./tablets-buscar.component.css']
})
export class TabletsBuscarComponent implements OnInit {
  listTablets: tablets[]=[];
  constructor(private _tabletsService: TabletsService){}

  ngOnInit(): void {
    this.obtenerTablets();
  }
  obtenerTablets(){
    this._tabletsService.getTablets().subscribe(data =>{
      console.log(data);
      this.listTablets=data;
    },error =>{
      console.log(error);
    })
  }
}
