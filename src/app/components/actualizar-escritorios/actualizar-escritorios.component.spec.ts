import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarEscritoriosComponent } from './actualizar-escritorios.component';

describe('ActualizarEscritoriosComponent', () => {
  let component: ActualizarEscritoriosComponent;
  let fixture: ComponentFixture<ActualizarEscritoriosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarEscritoriosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActualizarEscritoriosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
