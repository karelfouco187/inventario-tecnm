import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoresComponent } from './proyectores.component';

describe('ProyectoresComponent', () => {
  let component: ProyectoresComponent;
  let fixture: ComponentFixture<ProyectoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProyectoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
