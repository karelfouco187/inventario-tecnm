import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarEscritoriosComponent } from './agregar-escritorios.component';

describe('AgregarEscritoriosComponent', () => {
  let component: AgregarEscritoriosComponent;
  let fixture: ComponentFixture<AgregarEscritoriosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarEscritoriosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgregarEscritoriosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
