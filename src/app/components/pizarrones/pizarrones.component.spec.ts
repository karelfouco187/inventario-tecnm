import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizarronesComponent } from './pizarrones.component';

describe('PizarronesComponent', () => {
  let component: PizarronesComponent;
  let fixture: ComponentFixture<PizarronesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizarronesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizarronesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
