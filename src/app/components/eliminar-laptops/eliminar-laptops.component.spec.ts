import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarLaptopsComponent } from './eliminar-laptops.component';

describe('EliminarLaptopsComponent', () => {
  let component: EliminarLaptopsComponent;
  let fixture: ComponentFixture<EliminarLaptopsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminarLaptopsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EliminarLaptopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
