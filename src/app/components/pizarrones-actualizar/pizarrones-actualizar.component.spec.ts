import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizarronesActualizarComponent } from './pizarrones-actualizar.component';

describe('PizarronesActualizarComponent', () => {
  let component: PizarronesActualizarComponent;
  let fixture: ComponentFixture<PizarronesActualizarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizarronesActualizarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizarronesActualizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
