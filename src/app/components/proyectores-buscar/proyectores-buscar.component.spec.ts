import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoresBuscarComponent } from './proyectores-buscar.component';

describe('ProyectoresBuscarComponent', () => {
  let component: ProyectoresBuscarComponent;
  let fixture: ComponentFixture<ProyectoresBuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoresBuscarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProyectoresBuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
