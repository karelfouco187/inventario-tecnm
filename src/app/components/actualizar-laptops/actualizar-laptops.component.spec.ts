import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarLaptopsComponent } from './actualizar-laptops.component';

describe('ActualizarLaptopsComponent', () => {
  let component: ActualizarLaptopsComponent;
  let fixture: ComponentFixture<ActualizarLaptopsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarLaptopsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActualizarLaptopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
