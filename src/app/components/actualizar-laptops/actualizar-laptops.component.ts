import { Component, OnInit } from '@angular/core';
import { equipos } from 'src/app/models/equipos';
import { EquiposService } from 'src/app/services/equipos.service';
@Component({
  selector: 'app-actualizar-laptops',
  templateUrl: './actualizar-laptops.component.html',
  styleUrls: ['./actualizar-laptops.component.css']
})
export class ActualizarLaptopsComponent implements OnInit {
  listEquipos: equipos[] = [];
  ngOnInit(): void {
    this.obtenerEquipos();
  }
  constructor(private _equiposService: EquiposService){}
  obtenerEquipos() {
    this._equiposService.getEquipos().subscribe(data => {
      console.log(data);
      this.listEquipos = data;
    }, error => {
      console.log(error);
    })
  }
}
