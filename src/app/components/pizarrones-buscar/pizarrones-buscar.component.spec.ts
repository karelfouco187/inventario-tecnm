import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizarronesBuscarComponent } from './pizarrones-buscar.component';

describe('PizarronesBuscarComponent', () => {
  let component: PizarronesBuscarComponent;
  let fixture: ComponentFixture<PizarronesBuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizarronesBuscarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizarronesBuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
