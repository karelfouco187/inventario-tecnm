import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoresActualizarComponent } from './proyectores-actualizar.component';

describe('ProyectoresActualizarComponent', () => {
  let component: ProyectoresActualizarComponent;
  let fixture: ComponentFixture<ProyectoresActualizarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoresActualizarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProyectoresActualizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
