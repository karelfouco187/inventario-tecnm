import { Component,OnInit } from '@angular/core';
import { proyectores } from 'src/app/models/proyectores';
import { ProyectoresService } from 'src/app/services/proyectores.service';
@Component({
  selector: 'app-proyectores-actualizar',
  templateUrl: './proyectores-actualizar.component.html',
  styleUrls: ['./proyectores-actualizar.component.css']
})
export class ProyectoresActualizarComponent implements OnInit {
  listProyectores: proyectores[] = [];
  constructor(private _proyectoresService: ProyectoresService){}
  ngOnInit(): void {
    this.obtenerProyectores();
  }
  obtenerProyectores() {
    this._proyectoresService.getProyectores().subscribe(data => {
      console.log(data);
      this.listProyectores = data;
    }, error => {
      console.log(error);
    })
  }
}
