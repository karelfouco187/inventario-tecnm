import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoresEliminarComponent } from './proyectores-eliminar.component';

describe('ProyectoresEliminarComponent', () => {
  let component: ProyectoresEliminarComponent;
  let fixture: ComponentFixture<ProyectoresEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoresEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProyectoresEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
