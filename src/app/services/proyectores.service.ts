import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { proyectores } from '../models/proyectores';
@Injectable({
  providedIn: 'root'
})
export class ProyectoresService {
  url='http://localhost:4000/api/proyectors/'
  constructor(private http: HttpClient) { }

  getProyectores(): Observable<any>{
    return this.http.get(this.url);
  }

  eliminarProyectores(id:string): Observable<any>{
    return this.http.delete(this.url+id);
  }

  guardarProyectores(proyectores: proyectores): Observable<any>{
    return this.http.post(this.url, proyectores);
  }
  editarProyectores(id: string, proyectores: proyectores): Observable<any>{
    return this.http.put(this.url + id, proyectores);
  }
  obtenerProyectores(id: string): Observable<any>{
    return this.http.get(this.url + id);
  }
}
