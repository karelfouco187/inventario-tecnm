import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { equipos } from '../models/equipos';

@Injectable({
  providedIn: 'root'
})
export class EquiposService {
  url='http://localhost:4000/api/equipos/'
  constructor(private http: HttpClient) {  }

  getEquipos(): Observable<any>{
    return this.http.get(this.url);
  }

  eliminarEquipos(id:string): Observable<any>{
    return this.http.delete(this.url+id);
  }

  guardarEquipo(equipo: equipos): Observable<any>{
    return this.http.post(this.url, equipo);
  }
  editarEquipo(id: string, equipo: equipos): Observable<any>{
    return this.http.put(this.url + id, equipo);
  }
  obtenerEquipo(id: string): Observable<any>{
    return this.http.get(this.url + id);
  }
}
