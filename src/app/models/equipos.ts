export class equipos{
  _id?:number;
  serie: string;
  marca: string;
  procesador: string;
  ram: string;
  almacenamiento: string;

  constructor(serie:string, marca:string, procesador:string, ram:string, almacenamiento:string){
    this.serie=serie;
    this.marca=marca;
    this.procesador=procesador;
    this.ram=ram;
    this.almacenamiento=almacenamiento;
  }
}
