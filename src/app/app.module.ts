import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

import {RouterModule} from '@angular/router'
import {HttpClientModule} from '@angular/common/http'


import { ROUTES } from './app.routes';
import { MenuComponent } from './components/menu/menu.component';
import { LaptopsComponent } from './components/laptops/laptops.component';
import { TabletsComponent } from './components/tablets/tablets.component';
import { ProyectoresComponent } from './components/proyectores/proyectores.component';
import { EscritoriosComponent } from './components/escritorios/escritorios.component';
import { SillasComponent } from './components/sillas/sillas.component';
import { PizarronesComponent } from './components/pizarrones/pizarrones.component';
import { AgregarLaptopsComponent } from './components/agregar-laptops/agregar-laptops.component';
import { EliminarLaptopsComponent } from './components/eliminar-laptops/eliminar-laptops.component';
import { BuscarLaptopsComponent } from './components/buscar-laptops/buscar-laptops.component';
import { ActualizarLaptopsComponent } from './components/actualizar-laptops/actualizar-laptops.component';
import { AgregarEscritoriosComponent } from './components/agregar-escritorios/agregar-escritorios.component';
import { EliminarEscritoriosComponent } from './components/eliminar-escritorios/eliminar-escritorios.component';
import { BuscarEscritoriosComponent } from './components/buscar-escritorios/buscar-escritorios.component';
import { ActualizarEscritoriosComponent } from './components/actualizar-escritorios/actualizar-escritorios.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { TabletsAgregarComponent } from './components/tablets-agregar/tablets-agregar.component';
import { TabletsEliminarComponent } from './components/tablets-eliminar/tablets-eliminar.component';
import { TabletsActualizarComponent } from './components/tablets-actualizar/tablets-actualizar.component';
import { TabletsBuscarComponent } from './components/tablets-buscar/tablets-buscar.component';
import { ProyectoresActualizarComponent } from './components/proyectores-actualizar/proyectores-actualizar.component';
import { ProyectoresAgregarComponent } from './components/proyectores-agregar/proyectores-agregar.component';
import { ProyectoresBuscarComponent } from './components/proyectores-buscar/proyectores-buscar.component';
import { ProyectoresEliminarComponent } from './components/proyectores-eliminar/proyectores-eliminar.component';
import { SillasActualizarComponent } from './components/sillas-actualizar/sillas-actualizar.component';
import { SillasAgregarComponent } from './components/sillas-agregar/sillas-agregar.component';
import { SillasBuscarComponent } from './components/sillas-buscar/sillas-buscar.component';
import { SillasEliminarComponent } from './components/sillas-eliminar/sillas-eliminar.component';
import { PizarronesActualizarComponent } from './components/pizarrones-actualizar/pizarrones-actualizar.component';
import { PizarronesAgregarComponent } from './components/pizarrones-agregar/pizarrones-agregar.component';
import { PizarronesBuscarComponent } from './components/pizarrones-buscar/pizarrones-buscar.component';
import { PizarronesEliminarComponent } from './components/pizarrones-eliminar/pizarrones-eliminar.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    LaptopsComponent,
    TabletsComponent,
    ProyectoresComponent,
    EscritoriosComponent,
    SillasComponent,
    PizarronesComponent,
    AgregarLaptopsComponent,
    EliminarLaptopsComponent,
    BuscarLaptopsComponent,
    ActualizarLaptopsComponent,
    AgregarEscritoriosComponent,
    EliminarEscritoriosComponent,
    BuscarEscritoriosComponent,
    ActualizarEscritoriosComponent,
    RegistrarComponent,
    TabletsAgregarComponent,
    TabletsEliminarComponent,
    TabletsActualizarComponent,
    TabletsBuscarComponent,
    ProyectoresActualizarComponent,
    ProyectoresAgregarComponent,
    ProyectoresBuscarComponent,
    ProyectoresEliminarComponent,
    SillasActualizarComponent,
    SillasAgregarComponent,
    SillasBuscarComponent,
    SillasEliminarComponent,
    PizarronesActualizarComponent,
    PizarronesAgregarComponent,
    PizarronesBuscarComponent,
    PizarronesEliminarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES,{useHash:true}),
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
